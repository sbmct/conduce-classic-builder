#!/bin/bash -xe

scripts_dir=$(realpath $(dirname $0))

if [ $EUID -ne 0 ] ; then
    echo "Permission denied, please run as root"
    echo "sudo $0"
    exit 1
fi

IMAGE=$(realpath $1)
if [ ! -e $IMAGE ] ; then
    echo "ISO image file does not exist"
    exit 1
fi

DEVICE=$2
if [ "$DEVICE" == "" ] ; then
    DEVICE="sdb"
fi

REMASTER_PATH=$HOME/remaster
ROOT=$REMASTER_PATH/liveusb
mkdir -p $ROOT

function cleanup {
    cd $ROOT
    if mount | grep "liveusb/tmp" > /dev/null ; then
        umount -lf $ROOT/tmp
    fi
    if mount | grep "liveusb/mnt" > /dev/null ; then
        umount -lf $ROOT/mnt
    fi
    rm -f loop
    rm -rf mnt
    rm -rf tmp
}

cleanup

cd $ROOT

touch loop
dd if=/dev/zero of=loop bs=1 count=1 seek=10000M
mkfs.ext2 -F -F -L conduce-classic -m 0 loop

mkdir -p tmp
mount -o loop $IMAGE tmp

mkdir -p mnt
mount -o loop loop mnt

cp -a tmp/. mnt/

cd mnt
mkdir -p boot
mv isolinux boot/extlinux
mv boot/extlinux/isolinux.cfg boot/extlinux/extlinux.conf
extlinux --install boot/extlinux/
cd ..
umount mnt
umount tmp

REMIX=$(basename $IMAGE)
REMIX=${REMIX%.*}.gz
gzip -c loop > $REMIX 

$scripts_dir/write-usb.sh $REMIX $DEVICE

paplay /usr/share/sounds/freedesktop/stereo/complete.oga

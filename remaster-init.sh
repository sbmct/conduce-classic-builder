#!/bin/bash -xe
# Derived from: https://help.ubuntu.com/community/LiveCDCustomization

ISO=$HOME/iso/ubuntu-12.04.4-desktop-amd64.iso
if [ -e "$1" ] ; then
    ISO=$1
fi

if [ $EUID -ne 0 ] ; then
    echo "Permission denied, please run as root"
    echo "sudo $0"
    exit 1
fi

REMASTER_PATH=$HOME/remaster

apt-get update
apt-get install --yes syslinux squashfs-tools genisoimage extlinux realpath
mkdir -p $REMASTER_PATH/mnt
mkdir -p $REMASTER_PATH/extract-cd/{casper,isolinux,install}

if [ ! -e $REMASTER_PATH/mnt/casper/filesystem.squashfs ] ; then
    mount -o loop $ISO $REMASTER_PATH/mnt
    rsync --exclude=/casper/filesystem.squashfs -a $REMASTER_PATH/mnt/ $REMASTER_PATH/extract-cd

    unsquashfs $REMASTER_PATH/mnt/casper/filesystem.squashfs
    rm -rf $REMASTER_PATH/edit
    mv squashfs-root $REMASTER_PATH/edit
fi

$(dirname $0)/remaster-link.sh

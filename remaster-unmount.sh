#!/bin/bash

if [ $EUID -ne 0 ] ; then
    echo "Permission denied, please run as root"
    echo "sudo $0"
    exit 1
fi

REMASTER_PATH=$HOME/remaster

umount $REMASTER_PATH/mnt
umount $REMASTER_PATH/edit/run
umount $REMASTER_PATH/edit/dev

exit 0

#!/bin/bash -xe

IMAGE=$1
IMAGE=$(realpath $1)
if [ ! -e $IMAGE ] ; then
    echo "image file does not exist"
    exit 1
fi

DEVICE=$2
if [ "$DEVICE" == "" ] ; then
    DEVICE="sdb"
fi

if mount | grep -q "/dev/$DEVICE[1-9]\+" ; then
    umount /dev/$DEVICE?
fi
if mount | grep -q /dev/$DEVICE ; then
    umount /dev/$DEVICE
fi

zcat $IMAGE > /dev/$DEVICE
eject /dev/$DEVICE


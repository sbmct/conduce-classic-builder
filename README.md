# Conduce Classic Builder
## Overview
These scripts help build an installer image that can be copied to a CD or USB flash drive and used to install Conduce Classic on a PC.  The user must have an Ubuntu 12.04 system (VM) and an Ubuntu 12.04 desktop installation image (or other source image).

## Before we begin
- Ubuntu 12.04 system (may be a VirtualBox)
- Ubuntu 12.04 desktop installer image (ISO; may be stock or other, other flavors (such as server) are not supported by may work) 

##VirtualBox Setup Steps
1. Create a Linux 64-bit Ubuntu virtualbox image
2. Use ~10BG for the RAM
3. Create a virtual hard disk
4. Select VDI(virtualbox disk image), used Dynamically allocated Storage 
5. Use 50GB fot the image size
6. Open the settings for the new virtualbox just created. In the Network tab, change the network adapter to "bridged". This is necessary for the scp below.
7. Launch the virtualbox. Select an ubuntu iso to boot from. 
8. Install Ubuntu and restart the virtualbox
9. scp ubuntu-12.04.4-desktop-amd64.iso from your local machine onto the virtualbox machine.
10. Move the ubuntu into ~/iso on the virtualbox machine.
11. install git and clone the conduce classic repo (this repo)
12. run sudo ./unattended (see below)

## Howto

### TLDR
Stick a USB flash drive in your Ubuntu VM, make sure it is sdb and run
```
sudo ./unattended
```
from this repo. 
The last message should indicate that the /dev/sdb drive has been unmounted (by the script)

### Execution
#### remaster-init.sh
Takes one argument (source ISO, defaults to $HOME/iso/ubuntu-12.04.4-desktop-amd64.iso)

Initializes remaster "environment".  Creates directories, mounts the ISO and copies files to the "environment" paths.

#### remaster-mount.sh
Takes one argument (-u, don't chroot when done)

Mounts paths in chroot environment.  When finished the chroot environment is mounted (unless `-u` is specified).

##### initialize-chroot.sh
Prepares the chroot environment for configuration.  Mounts required devices, configures environment variables and overrides some system files.

##### configure-chroot.sh
Setup the conduce classic runtime environment.

- Don't prompt to update packages
- Setup hostname
- create conduce-classic user
- auto login
- disable screensaver
- install nvidia drivers, openssh server, git
- update python environment components
    - pip
    - setuptools
    - virtualenv
    - wheel
##### install-sluice-chroot.sh
- Define APT sources for conduce packages
- Install sluice
- Remove unused packages
- Install xorg
- Setup init.d to run sluice on login

##### cleanup-chroot.sh
- Rename ld.so.conf.d files to fix search order
- Clean APT cache
- Remove setup files
- Revert chroot environment

#### remaster-unmount.sh
Unmount things mounted in remaster-mount.sh

#### build-iso.sh
Build an ISO from the chroot environment

#### make-usb.sh
Takes two arguments (source ISO, device name (defaults to sdb))

Converts ISO to bootable USB flash.

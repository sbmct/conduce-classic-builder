#!/bin/bash

gsettings set org.gnome.settings-daemon.plugins.power sleep-display-ac 0
gsettings set org.gnome.settings-daemon.plugins.power sleep-display-battery 0
gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-ac-timeout 0
gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-battery-timeout 0
gsettings set org.gnome.settings-daemon.plugins.power active 'false'
gsettings set org.gnome.desktop.screensaver lock-enabled 'false'
gsettings set org.gnome.desktop.screensaver logout-enabled 'false'
gsettings set org.gnome.desktop.screensaver ubuntu-lock-on-suspend 'false'
gsettings set org.gnome.desktop.screensaver user-switch-enabled 'false'
gsettings set org.gnome.desktop.screensaver idle-activation-enabled 'false'
gsettings set org.gnome.desktop.session idle-delay 0

xset -dpms s off
xset s noblank

#!/bin/bash -xe

if [ $EUID -ne 0 ] ; then
    echo "Permission denied, please run as root"
    echo "Make sure you have run chroot"
    exit 1
fi

mv /etc/ld.so.conf.d/libc.conf /etc/ld.so.conf.d/z_libc.conf
mv /etc/ld.so.conf.d/conduce.conf /etc/ld.so.conf.d/zz_conduce.conf
ldconfig

#mkinitramfs -c lzma -o /initrd.lz 3.11.0-15-generic

if [ -e /sbin/initctl.orig ] ; then
    rm /sbin/initctl
    dpkg-divert --rename --remove /sbin/initctl
    mv /sbin/initctl.orig /sbin/initctl
fi

#Remove upgraded, old kernels if more than one
#ls /boot/vmlinuz-[0-9]*.[0-9]*.[0-9]*-[0-9]*-generic > list.txt
#sum=$(cat list.txt | grep '[^ ]' | wc -l)

#if [ $sum -gt 1 ]; then
#    dpkg -l 'linux-*' | sed '/^ii/!d;/'"$(uname -r | sed "s/\(.*\)-\([^0-9]\+\)/\1/")"'/d;s/^[^ ]* [^ ]* \([^ ]*\).*/\1/;/[0-9]/!d' | xargs apt-get -y purge
#fi

#rm -f list.txt

apt-get clean

rm -rf /tmp/*
rm -f /etc/resolv.conf
rm -f /var/lib/dbus/machine-id
rm -f /var/run/dbus/pid
rm -f /home/conduce-classic/.bash_history

chown -R conduce-classic:conduce-classic /home/conduce-classic

/root/unmount-chroot.sh
rm -rf /root

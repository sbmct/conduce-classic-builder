#!/bin/bash -xe
# Derived from: https://help.ubuntu.com/community/LiveCDCustomization

root=$(realpath $(dirname $0))

function disable-screensaver-and-power-management {
    cp $root/disable-screensaver-and-power-management.sh /home/conduce-classic/.profile-disable-screensaver-and-power-management
    chmod 755 /home/conduce-classic/.profile-disable-screensaver-and-power-management
    if ! grep -q '.profile-disable-screensaver-and-power-management' /home/conduce-classic/.profile ; then
        echo '' >> /home/conduce-classic/.profile
        echo '$HOME/.profile-disable-screensaver-and-power-management' >> /home/conduce-classic/.profile
    fi
}

function enable-autologin {
    sed -i '/autologin/d' /etc/lightdm/lightdm.conf
    echo "autologin-user=conduce-classic" >> /etc/lightdm/lightdm.conf
    echo "autologin-user-timeout=0" >> /etc/lightdm/lightdm.conf
}

function set-admin-su-permissions {
    if ! grep -q 'conduce-admin' /etc/pam.d/su ; then
        sed -i '/auth       sufficient pam_rootok.so/a auth       [success=ignore default=1] pam_succeed_if.so user = conduce-classic\nauth       sufficient pam_succeed_if.so use_uid user = conduce-admin' /etc/pam.d/su
    fi
}

if [ $EUID -ne 0 ] ; then
    echo "Permission denied, please run as root"
    echo "Make sure you have run chroot"
    exit 1
fi

if ! getent passwd conduce-classic &> /dev/null ; then
    useradd -s /bin/bash -p $(cat $root/conduce-classic.passwd) conduce-classic
    mkdir -p /home/conduce-classic
    chown conduce-classic:conduce-classic /home/conduce-classic
fi
if ! getent passwd conduce-admin &> /dev/null ; then
    useradd -s /bin/bash -p $(cat $root/conduce-admin.passwd) conduce-admin -G sudo
    mkdir -p /home/conduce-admin
    chown conduce-admin:conduce-admin /home/conduce-admin
fi

set-admin-su-permissions

sed -i 's/Prompt=lts/Prompt=never/' /etc/update-manager/release-upgrades
sed -i 's/^send host-name.*/send host-name "conduce-classic"/' /etc/dhcp/dhclient.conf
sed -i 's/ubuntu/conduce-classic/' /etc/casper.conf
sed -i 's/ubuntu/conduce-classic/' /etc/hosts
echo 'conduce-classic' > /etc/hostname

enable-autologin

disable-screensaver-and-power-management

apt-get update

apt-get remove --yes --purge libreoffice*
apt-get remove --yes --purge rhythmbox*
apt-get remove --yes --purge software-center
apt-get remove --yes --purge ubuntuone*
apt-get remove --yes --purge compiz
apt-get remove --yes --purge gnome-screensaver
apt-get remove --yes --purge gnome-power-manager
apt-get remove --yes --purge telepathy*
apt-get remove --yes --purge thunderbird*


apt-get upgrade --yes
#official instructions have apt-get dbus
#apt-get install --yes dbus

apt-get install --yes openssh-server realpath git nvidia-340 apt-transport-https curl 

#NOTE:  THIS WOULD NEED TO BE DONE POST SLUICE INSTALL
#pip install --upgrade pip
#pip install --upgrade setuptools
#pip install --upgrade virtualenv
#pip install --upgrade wheel

#!/bin/bash -xe

if [ $EUID -ne 0 ] ; then
    echo "Permission denied, please run as root"
    echo "Make sure you have run chroot"
    exit 1
fi

apt-get -y install build-essential zlib1g-dev libssl-dev libreadline6-dev libyaml-dev
cd /tmp
wget http://cache.ruby-lang.org/pub/ruby/2.0/ruby-2.0.0-p481.tar.gz
tar -xvzf ruby-2.0.0-p481.tar.gz
cd ruby-2.0.0-p481/
./configure --prefix=/usr/local
make
make install

#!/bin/bash -xe

script_dir=$(realpath $(dirname $0))

setup-apt-repository()
{
    if ! apt-key list | grep -q conduce ; then
        curl https://mctdev:HcyzA4AYWsEah35R@apt.mct.io/CONDUCE-REPOSITORY-GPG-KEY.gpg | apt-key add -
    fi
    if [ ! -e  /etc/apt/sources.list.d/conduce.list ] ; then
        echo "deb https://mctdev:HcyzA4AYWsEah35R@apt.mct.io/ develop main" | tee /etc/apt/sources.list.d/conduce.list
    fi
}

setup-pip-conf()
{
    if ! grep -q conduce /home/conduce-classic/.pip/pip.conf ; then
        mkdir -p $HOME/.pip
        mkdir -p /home/conduce-classic/.pip
        echo "[install]" > $HOME/.pip/pip.conf
        echo "extra-index-url = https://mctdev:HcyzA4AYWsEah35R@pip.mct.io/develop/" >> $HOME/.pip/pip.conf
        if [ "$HOME" != "/home/conduce-classic" ] ; then
            cp $HOME/.pip/pip.conf /home/conduce-classic/.pip/pip.conf
        fi
    fi
}

setup-conduce-webtools()
{
    if [ -e ${script_dir}/conduce-webtools ] ; then
        mv ${script_dir}/conduce-webtools /opt/mct/sluice/webtools/scripts/
        chmod 755 /opt/mct/sluice/webtools/scripts/conduce-webtools
    fi
    ln -sf /opt/mct/sluice/webtools/scripts/conduce-webtools /etc/init.d/conduce-webtools
}

setup-conduce-classic()
{
    if [ -e ${script_dir}/conduce-classic ] ; then
        mv ${script_dir}/conduce-classic /opt/mct/sluice/webtools/scripts/
        chmod 755 /opt/mct/sluice/webtools/scripts/conduce-classic
    fi
    ln -sf /opt/mct/sluice/webtools/scripts/conduce-classic /etc/init.d/conduce-classic
    ln -sf /etc/init.d/conduce-classic /etc/rc0.d/K08conduce-classic
    ln -sf /etc/init.d/conduce-classic /etc/rc2.d/S08conduce-classic
}


setup-conduce-nginx()
{
    if [ -e ${script_dir}/conduce-nginx ] ; then
        mv ${script_dir}/conduce-nginx /opt/mct/sluice/webtools/scripts/
        chmod 755 /opt/mct/sluice/webtools/scripts/conduce-nginx
    fi
    ln -sf /opt/mct/sluice/webtools/scripts/conduce-nginx /etc/init.d/conduce-nginx
}


setup-conduce-pool-server()
{
    if [ -e ${script_dir}/conduce-pool-server ] ; then
        mv ${script_dir}/conduce-pool-server /opt/mct/sluice/webtools/scripts/
        chmod 755 /opt/mct/sluice/webtools/scripts/conduce-pool-server
    fi
    ln -sf /opt/mct/sluice/webtools/scripts/conduce-pool-server /etc/init.d/conduce-pool-server
}

if [ $EUID -ne 0 ] ; then
    echo "Permission denied, please run as root"
    echo "sudo $0"
    exit 1
fi

if [ ! -d /home/conduce-classic ] ; then
    echo "create conduce-classic user"
    exit 2
fi 

setup-apt-repository

apt-get --yes update

if ! ruby --version | grep -q "ruby 2.0" ; then
    $script_dir/install-ruby-chroot.sh
fi

apt-get --yes install sluice

if [ ! -e /opt/mct/sluice/webtools/scripts/conduce-webtools -a ! -e ${script_dir}/conduce-webtools ] ; then
    echo "script conduce-webtools not found"
    exit 3
fi
if [ ! -e /opt/mct/sluice/webtools/scripts/conduce-nginx -a ! -e ${script_dir}/conduce-nginx ] ; then
    echo "script conduce-nginx not found"
    exit 3
fi
if [ ! -e /opt/mct/sluice/webtools/scripts/conduce-pool-server -a ! -e ${script_dir}/conduce-pool-server ] ; then
    echo "script conduce-pool-server not found"
    exit 3
fi

apt-get --yes autoremove --purge
apt-get --yes install xorg

${script_dir}/patches/apply-patches.sh

setup-pip-conf
pip install --upgrade pip virtualenv setuptools

setup-conduce-classic
setup-conduce-webtools
setup-conduce-nginx
setup-conduce-pool-server

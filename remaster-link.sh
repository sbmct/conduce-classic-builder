#!/bin/bash -xe
# Derived from: https://help.ubuntu.com/community/LiveCDCustomization

if [ $EUID -ne 0 ] ; then
    echo "Permission denied, please run as root"
    echo "sudo $0"
    exit 1
fi

REMASTER_PATH=$HOME/remaster

if [ -e $REMASTER_PATH/edit/etc/apt/sources.list ] ; then
    mv $REMASTER_PATH/edit/etc/apt/sources.list $REMASTER_PATH/edit/etc/apt/sources.list.orig
elif [ ! -d $REMASTER_PATH/edit/etc/apt ] ; then
    mkdir -p $REMASTER_PATH/edit/etc/apt
fi
cp $(dirname $0)/sources.list $REMASTER_PATH/edit/etc/apt/sources.list

mkdir -p $REMASTER_PATH/edit/root
ln -f $(realpath $(dirname $0))/*-chroot.sh $REMASTER_PATH/edit/root/
ln -f $(realpath $(dirname $0))/disable-screensaver-and-power-management.sh $REMASTER_PATH/edit/root/
ln -f $(realpath $(dirname $0))/conduce* $REMASTER_PATH/edit/root/
ln -f $(realpath $(dirname $0))/*.passwd $REMASTER_PATH/edit/root/
mkdir $REMASTER_PATH/edit/root/patches
ln -f $(realpath $(dirname $0))/patches/* $REMASTER_PATH/edit/root/patches/

#!/bin/bash -xe
# Derived from: https://help.ubuntu.com/community/LiveCDCustomization

if [ $EUID -ne 0 ] ; then
    echo "Permission denied, please run as root"
    echo "Make sure you have run chroot"
    exit 1
fi

if ! mount | grep proc ; then
mount -t proc none /proc
fi
if ! mount | grep sysfs ; then
mount -t sysfs none /sys
fi
if ! mount | grep devpts ; then
mount -t devpts none /dev/pts
fi

export HOME=/root
export LC_ALL=C

if [ ! -e /var/lib/dbus/machine-id ] ; then
    dbus-uuidgen > /var/lib/dbus/machine-id
fi

if [ -e /sbin/initctl -a ! -e /sbin/initctl.orig ] ; then
    mv /sbin/initctl /sbin/initctl.orig
    dpkg-divert --local --rename --add /sbin/initctl
    ln -s /bin/true /sbin/initctl
fi

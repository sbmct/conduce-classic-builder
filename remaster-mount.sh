#!/bin/bash -e
# Derived from: https://help.ubuntu.com/community/LiveCDCustomization

if [ $EUID -ne 0 ] ; then
    echo "Permission denied, please run as root"
    echo "sudo $0"
    exit 1
fi

REMASTER_PATH=$HOME/remaster

#This may be optional (enables name resolution I think???)
mount -o bind /run/ $REMASTER_PATH/edit/run

mount --bind /dev/ $REMASTER_PATH/edit/dev

if [ "$1" != "-u" ] ; then
    chroot $REMASTER_PATH/edit
fi

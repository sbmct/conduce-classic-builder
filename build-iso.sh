#!/bin/bash -xe
# Derived from: https://help.ubuntu.com/community/LiveCDCustomization

ISOLINUX_CFG="txt.cfg"
CFG="default"
SEED_FILE="unattended.seed"

REMASTER_PATH=$HOME/remaster

function rebuild_initrd {
    cd $REMASTER_PATH/extract-cd/casper
    mkdir lztempdir
    cd lztempdir
    lzma -dc -S .lz ../initrd.lz | cpio -imvd --no-absolute-filenames
    rm -f ../initrd.lz
    find . | cpio --quiet --dereference -o -H newc | lzma -7 > ../initrd.lz
    cd ..
    rm -rf lztempdir

}

function usage {
    echo "Usage: $0 [--cfg={default|dhl}]"
}

function parse_command_line {
    while [ "x$1" != "x" ] ; do
        case "$1" in
            help|--help|-h|"-?") cmd=usage ;;
            --cfg=*) CFG="${1#*=}" ; echo "cfg is $CFG" ;;
            *) >&2 echo "Unrecognized option: $1" ; usage ; exit 1 ;;
        esac
        shift
    done
}

function set_configuration_options {
    cfg=$1
    case "$cfg" in
        dhl)
            ISOLINUX_CFG="dhl.cfg"
            SEED_FILE="dhl.seed"
            ;;
        carp)
            ISOLINUX_CFG="carp.cfg"
            SEED_FILE="carp.seed"
            ;;
        kevnet)
            ISOLINUX_CFG="kevnet.cfg"
            SEED_FILE="kevnet.seed"
            ;;
        default) ;;
        *)
            >&2 echo "Unknown configuration: $cfg"
            usage
            exit 1
            ;;
    esac
}

if [ $EUID -ne 0 ] ; then
    echo "Permission denied, please run as root"
    echo "sudo $0"
    exit 1
fi

parse_command_line "$@"
set_configuration_options $CFG

#these should not be mounted
$(dirname $0)/remaster-unmount.sh

rm -f $REMASTER_PATH/edit/usr/lib/ubiquity/plugins/ubi-usersetup.py

sed -i '/ubiquity\/reboot/d' $REMASTER_PATH/extract-cd/preseed/ubuntu.seed
echo 'd-i ubiquity/reboot boolean true' >> $REMASTER_PATH/extract-cd/preseed/ubuntu.seed
cp $(dirname $0)/unattended.seed $REMASTER_PATH/extract-cd/preseed/
cp $(dirname $0)/$SEED_FILE $REMASTER_PATH/extract-cd/preseed/

cp $(dirname $0)/conduce-splash.png $REMASTER_PATH/extract-cd/isolinux/splash.png
cp $(dirname $0)/conduce-splash.pcx $REMASTER_PATH/extract-cd/isolinux/splash.pcx

#if [ -e $REMASTER_PATH/edit/initrd.lz ] ; then
#    mv $REMASTER_PATH/edit/initrd.lz $REMASTER_PATH/extract-cd/casper/initrd.lz
#    rebuild_initrd
#fi

sed -i '/default/d' $REMASTER_PATH/extract-cd/isolinux/isolinux.cfg
rm -f $REMASTER_PATH/extract-cd/isolinux/txt.cfg
cp $(dirname $0)/$ISOLINUX_CFG $REMASTER_PATH/extract-cd/isolinux/txt.cfg

if [ -e $REMASTER_PATH/edit/boot/vmlinuz-*.*.*-**-generic ] ; then
    cp $REMASTER_PATH/edit/boot/vmlinuz-*.*.*-**-generic $REMASTER_PATH/extract-cd/casper/vmlinuz
fi
cp $REMASTER_PATH/edit/boot/initrd.img-*.*.*-**-generic $REMASTER_PATH/extract-cd/casper/initrd.lz
#cp $REMASTER_PATH/edit/usr/lib/syslinux/isolinux.bin $REMASTER_PATH/extract-cd/isolinux/
#cp $REMASTER_PATH/edit/boot/memtest86+.bin $REMASTER_PATH/extract-cd/install/memtest

#cp $(dirname $0)/isolinux.txt $REMASTER_PATH/extract-cd/isolinux/
#cp $(dirname $0)/splash.rle $REMASTER_PATH/extract-cd/isolinux/

# Regenerate manifest
chmod +w $REMASTER_PATH/extract-cd/casper/filesystem.manifest
chroot $REMASTER_PATH/edit dpkg-query -W --showformat='${Package} ${Version}\n' > $REMASTER_PATH/extract-cd/casper/filesystem.manifest
cp $REMASTER_PATH/extract-cd/casper/filesystem.manifest $REMASTER_PATH/extract-cd/casper/filesystem.manifest-desktop
REMOVE='ubiquity ubiquity-frontend-gtk ubiquity-frontend-kde casper lupin-casper live-initramfs user-setup discover1 xresprobe os-prober libdebian-installer4'
for i in $REMOVE ; do
    sed -i '/${i}/d' $REMASTER_PATH/extract-cd/casper/filesystem.manifest-desktop
done

# compress filesystem
rm -f $REMASTER_PATH/extract-cd/casper/filesystem.squashfs
mksquashfs $REMASTER_PATH/edit $REMASTER_PATH/extract-cd/casper/filesystem.squashfs

# update filesystem.size
printf $(du -sx --block-size=1 $REMASTER_PATH/edit | cut -f1) > $REMASTER_PATH/extract-cd/casper/filesystem.size

if ! grep "Conduce Classic" $REMASTER_PATH/extract-cd/README.diskdefines ; then 
    sed -i 's/DISKNAME  \(.*\)/DISKNAME  Conduce Classic/' $REMASTER_PATH/extract-cd/README.diskdefines
fi

touch $REMASTER_PATH/extract-cd/ubuntu
mkdir -p $REMASTER_PATH/extract-cd/.disk
cd $REMASTER_PATH/extract-cd/.disk
touch base_installable
echo "full_cd/single" > cd_type
echo "Ubuntu Remix 12.04" > info  # Update version number to match your OS version
echo "http://dist.mct.io" > release_notes_url

cd $REMASTER_PATH/extract-cd

# set new md5sum
rm -f md5sum.txt
find . -type f -print0 | xargs -0 md5sum > md5sum.txt

# generat iso info
date=`date +%Y%m%d-%H%M%S`
volume="conduce-classic-$date"
filename="$volume-$CFG.iso"

# save iso info
echo "Conduce Classic" > README.conduce
echo "Date: $date" >> README.conduce
echo "Configuration: $CFG" >> README.conduce
echo "" >> README.conduce
echo "This is a live OS and installer for Conduce Classic based on Ubuntu 12.04" >> README.conduce

mkisofs -r -V "$volume" -volset="$CFG" -cache-inodes -J -l -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -o ../$filename . 
echo $(realpath ../$filename)

paplay /usr/share/sounds/freedesktop/stereo/complete.oga

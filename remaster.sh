#!/bin/bash -e
# Derived from: https://help.ubuntu.com/community/LiveCDCustomization

if [ $EUID -ne 0 ] ; then
    echo "Permission denied, please run as root"
    echo "sudo $0"
    exit 1
fi

REMASTER_PATH=$HOME/remaster

apt-get install squashfs-tools genisoimage
mkdir -p $REMASTER_PATH/mnt
mkdir -p $REMASTER_PATH/extract-cd

mount -o loop $HOME/iso/ubuntu-12.04.4-desktop-amd64.iso $REMASTER_PATH/mnt
rsync --exclude=/casper/filesystem.squashfs -a $REMASTER_PATH/mnt/ $REMASTER_PATH/extract-cd

unsquashfs $REMASTER_PATH/mnt/casper/filesystem.squashfs
mv squashfs-root $REMASTER_PATH/edit

#This may be optional (enables name resolution I think???)
mount -o bind /run/ $REMASTER_PATH/edit/run

mount --bind /dev/ $REMASTER_PATH/edit/dev

mv $REMASTER_PATH/edit/etc/apt/sources.list $REMASTER_PATH/edit/etc/apt/sources.list.orig
cp /etc/apt/sources.list $REMASTER_PATH/edit/etc/apt/sources.list
ln $(dirname $0)/remaster-chroot.sh $REMASTER_PATH/edit/root/remaster-chroot.sh
ln $(dirname $0)/install_sluice.sh $REMASTER_PATH/edit/root/install_sluice.sh

chroot $REMASTER_PATH/edit
#mount -t proc none /proc
#mount -t sysfs none /sys
#mount -t devpts none /dev/pts

#export HOME=/root
#export LC_ALL=C

#dbus-uuidgen > /var/lib/dbus/machine-id
#dpkg-divert --local --rename --add /sbin/initctl
#ln -s /bin/true /sbin/initctl

#apt-get install openssh-server
#apt-get install git
